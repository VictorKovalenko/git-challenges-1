import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'app',
      theme: ThemeData.dark(),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [],
        ),
      ),
      body: Row(
        children: [
          Container(
            width: 411.4,
            child: ListView(
              children: [
                Container(
                  height: 350,
                  color: Colors.white,
                ),
                Container(
                  height: 350,
                  color: Colors.white30,
                ),
                Container(
                  height: 350,
                  color: Colors.white10,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
